### Stability tests for cpu and gpu

## Software
# stress-ng, gputest
# mangohud optional

echo "Stability tests for cpu and gpu"
echo
echo "The following programs must be installed for the tests to work: stress-ng, gputest"
echo "https://www.geeks3d.com/gputest/" 
echo "https://github.com/ColinIanKing/stress-ng"
echo
echo "Attention! The temperatures rise massively during the tests. Make sure that there is sufficient cooling!"
echo
read -p "Continue? (y/n) " response
if [ "$response" = "y" ]; then

while [[ 1 ]] ; do
        echo -n " How many minutes should the tests run? "
    read minutes
    if [[ "$minutes" =~ ^[0-9]+$ ]] ; then
        break
    fi
done

# Save values before testing; remove unnecessary entries from sensors
sensors | grep : | grep -E '^[^:]+' | sed 's/([^>]*)//g' | sed '/Adapter/d' | sed '/intrusion/d' | sed '/beep/d' >> ~/tests_sensors_defaults.txt

# Check if mangohud is available; start furmark
if type mangohud > /dev/null 2>&1; then
mangohud gputest /test=fur /fullscreen &
else
gputest /test=fur /fullscreen &
fi

# start stress-ng on all cores
stress-ng --matrix -1 -t $minutes"m" >> ~/tests_stressng_results.txt &

# Run for X minutes. Save sensors_data and cpu_frequency to files
sleep $minutes"m"
cat /proc/cpuinfo | grep "MHz" | sed -r 's/.{4}$//' | column -tc 4 | columns >> ~/tests_freq.cpu.txt
sensors | grep : | grep -E '^[^:]+' | sed 's/([^>]*)//g' | sed '/Adapter/d' | sed '/intrusion/d' | sed '/beep/d' >> ~/tests_sensors.txt

# Kill gputest
killall GpuTest gputest_gui.py gputest

# summary
clear
echo "Tests ended after $minutes minutes"
echo
echo "Cores speed before test ended:"
cat ~/tests_freq.cpu.txt
echo
echo "Stress-ng output:"
tail -n3 ~/tests_stressng_results.txt
echo
echo
sed -i '1i Values before testing:' ~/tests_sensors_defaults.txt
sed -i '1i Values after testing:' ~/tests_sensors.txt
diff -y --suppress-common-lines $1 $2 ~/tests_sensors_defaults.txt ~/tests_sensors.txt

## Remove files
rm -f ~/tests_sensors.txt
rm -f ~/tests_freq.cpu.txt
rm -f ~/tests_sensors_defaults.txt
rm -f ~/tests_stressng_results.txt

else
exit
fi
