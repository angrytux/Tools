# Tools

*Various scripts, hints and tips for Linux things :-)*
<br>
<br>
#### Global microphone toggle for mute/unmute (hotkey). Particularly useful for plasma wayland:

https://codeberg.org/angrytux/Tools/src/branch/main/Mic_Toggle.sh
<br>
<br>
#### Automated stability tests for graphics card and processor:

https://codeberg.org/angrytux/Tools/src/branch/main/Testing.sh
<br>
<br>
<br>

### **For tips and hints refer to the [Wiki](https://codeberg.org/angrytux/Tools/wiki)**
