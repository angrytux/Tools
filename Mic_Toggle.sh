#!/bin/bash

### global microphone toggle script
### Icons are optional but beautiful :-)

## Requirements:
## pipewire and wireplumber (wpctl); libnotify for notifications (notify-send)
## Optional yad for tray icon: http://aur.archlinux.org/packages/yad-gtk2

# check if wireplumber is installed. Exit if not.
if ! command -v wpctl &> /dev/null
then
    echo "wpctl could not be found. Script does nothing useful without wireplumber installed."
    exit
fi

# check if notify-send is installed
if ! command -v notify-send &> /dev/null
then
echo "notify-send could not be found. Toggle will work but without notifications shown."
fi

# main
status=$(wpctl get-volume @DEFAULT_AUDIO_SOURCE@ | grep MUTE) #check status of default sink

if [ -z "$status" ] 
then
mumble rpc mute #optional for mumble status
notify-send --icon=/path/icon.png -a "Microphone status" 'muted'
wpctl set-mute @DEFAULT_AUDIO_SOURCE@ 1 #mute
yad --notification --image="/path/microphone-muted.png" & #continue script
play -v 0.1 /path/.mic_muted.wav
exit
else
mumble rpc unmute #optional for mumble status
notify-send --icon=/path/icon2.png -a "Microphone status" 'unmuted'
wpctl set-mute @DEFAULT_AUDIO_SOURCE@ 0 #unmute
pkill yad #kill tray icon quick and dirty
play -v 0.1 /path/.mic_unmuted.wav
exit
fi


 
